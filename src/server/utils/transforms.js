const orderFactories = (rows) => {
  const orderedFactoriesTmp = {};
  const orderedFactories = [];
  rows.forEach((row) => {
    if (!orderedFactoriesTmp[row.id]) {
      orderedFactoriesTmp[row.id] = {
        name: row.name,
        updated_at: row.updated_at,
        nodes: [{ value: row.node, id: row.node_id }],
      };
    } else {
      orderedFactoriesTmp[row.id].nodes.push({ value: row.node, id: row.node_id });
    }
  });

  Object.keys(orderedFactoriesTmp).forEach((key) => {
    const { name, updated_at: updatedAt, nodes } = orderedFactoriesTmp[key];
    orderedFactories.push({
      id: key,
      name,
      updatedAt,
      nodes,
    });
  });

  return orderedFactories;
};

const generateNodes = (body) => {
  const nodes = [];

  for (let i = 0; i < body.nodeCount; i += 1) {
    nodes.push(Math.floor(Math.random() * body.high) + body.low);
  }

  return {
    name: body.name,
    nodes,
    removed: body.removed,
    added: nodes,
  };
};

module.exports = {
  orderFactories,
  generateNodes,
};
