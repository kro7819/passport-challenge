const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const pino = require('express-pino-logger')();
const path = require('path');
const factoryRoutes = require('./routes/factories');

const app = express();

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(cors());
app.use(pino);

// routes
app.use('/api/factories', factoryRoutes);

app.get('/*', (req, res) => {
  res.sendFile(path.join(process.cwd(), '/public/index.html'));
});

app.listen(process.env.PORT || 4000);
