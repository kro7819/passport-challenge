const express = require('express');
const queries = require('../../db/queries');
const transforms = require('../utils/transforms');

const factoryRouter = express.Router();

// get all factories
factoryRouter.get('/', (req, res) => (
  queries.factory.getAll()
    .then(([result]) => {
      if (result.length === 0) res.status(209).send({ success: true }).end();
      req.log.info(result);
      res.status(200);
      res.send(transforms.orderFactories(result));
    })
    .catch((err) => {
      req.log.info(err);
      res.status(500);
      res.send(err);
    })
));

// create new factory
factoryRouter.post('/', (req, res) => {
  const { name, nodes } = transforms.generateNodes(req.body);

  return queries.factory.create(name, nodes)
    .then((id) => {
      res.status(200);
      res.send({ id });
    })
    .catch((err) => {
      req.log.info(err);
      if (err && err.errno === 1062) {
        return res.status(409).send('Factory with name already exists');
      }
      return res.status(500).end();
    });
});

// get factory by id
factoryRouter.get('/:id', (req, res) => {
  const { id } = req.params;

  return queries.factory.getById(id)
    .then(([result]) => {
      if (result.length === 0) res.status(204).end();
      req.log.info(result);
      res.status(200);
      res.send(transforms.orderFactories(result));
    })
    .catch((err) => {
      req.log.info(err);
      res.status(500);
      res.send(err);
    });
});

// update factory by id
factoryRouter.put('/:id', (req, res) => {
  const { id } = req.params;
  const { body } = req;

  const transformed = transforms.generateNodes(body);
  console.log(transformed);

  return queries.factory.updateById(id, transformed)
    .then(() => {
      res.send({ success: true }).end();
    })
    .catch((err) => {
      req.log.info(err);
      if (err && err.errno === 1062) {
        return res.status(409).send('Factory with name already exists');
      }

      return res.status(500).end();
    });
});

// delete factory by id
factoryRouter.delete('/:id', (req, res) => {
  const { id } = req.params;

  return queries.factory.deleteById(id)
    .then(() => {
      res.send({ success: true }).end();
    })
    .catch((err) => {
      req.log.info(err);
      res.status(500);
      res.send(err);
    });
});

module.exports = factoryRouter;
