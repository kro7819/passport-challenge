import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Home from './views/Home';
import { setFactories, clearNotification } from './stores/actions';
import Factories from './views/Factories';
import Notification from './components/Notification';


class App extends Component {
  componentDidMount() {
    this.props.setFactories();
  }

  closeNotification() {
    this.props.clearNotification();
  }

  render() {
    return (
      <Router>
        <div className="main">
          <div className="nav">
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/factories">Dashboard</Link>
              </li>
            </ul>
          </div>

          <Route exact path="/" component={Home} />
          <Route exact path="/factories" component={Factories} />
          {this.props.error && (
            <Notification
              close={this.props.clearNotification}
              error={this.props.error}
              message={this.props.errorMsg}
            />
          )}
          {this.props.success && (
            <Notification
              close={this.props.clearNotification}
              error={this.props.error}
              message={this.props.successMsg}
            />
          )}
        </div>
      </Router>
    );
  }
}

const mapStateToProps = state => ({
  error: state.error,
  errorMsg: state.errorMsg,
  success: state.success,
  successMsg: state.successMsg,
});

const mapDispatchToProps = dispatch => ({
  setFactories: bindActionCreators(setFactories, dispatch),
  clearNotification: bindActionCreators(clearNotification, dispatch),
});

App.propTypes = {
  setFactories: PropTypes.func.isRequired,
  clearNotification: PropTypes.func.isRequired,
  error: PropTypes.bool.isRequired,
  errorMsg: PropTypes.string.isRequired,
  success: PropTypes.bool.isRequired,
  successMsg: PropTypes.string.isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
