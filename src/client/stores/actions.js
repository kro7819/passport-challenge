import api from '../utils/api';
import * as actionTypes from './actionTypes';

// ui
export const selectFactory = factory => dispatch => (
  dispatch({
    type: actionTypes.SELECT_FACTORY,
    data: factory,
  })
);

export const setFactoryName = name => dispatch => (
  dispatch({
    type: actionTypes.SET_FACTORY_NAME,
    data: name,
  })
);

export const clearNotification = () => dispatch => (
  dispatch({
    type: actionTypes.CLEAR_NOTIFICATION,
  })
);

export const setError = msg => (dispatch) => {
  dispatch({
    type: actionTypes.SET_ERROR,
    message: msg,
  });
};

export const setSuccess = msg => (dispatch) => {
  dispatch({
    type: actionTypes.SET_SUCCESS,
    message: msg,
  });
};

// api
export const setFactories = () => (dispatch) => {
  api.getFactories()
    .then((res) => {
      dispatch({
        type: actionTypes.SET_FACTORIES,
        data: res,
      });
    })
    .catch(err => err);
};

export const deleteFactory = id => (dispatch) => {
  api.deleteFactory(id)
    .then(() => {
      dispatch(setSuccess('Factory successfully deleted'));
      return api.getFactories();
    })
    .then((res) => {
      dispatch({
        type: actionTypes.SET_FACTORIES,
        data: res,
      });
    })
    .catch((err) => {
      dispatch(setError(err.message));
    });
};

export const createFactory = body => (dispatch) => {
  api.createFactory(body)
    .then(() => {
      dispatch(setSuccess(`Factory ${body.name} was successfully created`));
      return api.getFactories();
    })
    .then((res) => {
      dispatch({
        type: actionTypes.SET_FACTORIES,
        data: res,
      });
    })
    .catch((err) => {
      dispatch(setError(err.message));
    });
};

export const updateFactory = (id, body) => (dispatch) => {
  api.updateFactory(id, body)
    .then(() => {
      dispatch(setSuccess(`Factory ${body.name} was successfully updated`));
      return api.getFactories();
    })
    .then((res) => {
      dispatch({
        type: actionTypes.SET_FACTORIES,
        data: res,
      });
    })
    .catch((err) => {
      dispatch(setError(err.message));
    });
};
