import * as actionTypes from './actionTypes';

const initialState = {
  factories: [],
  selectedFactory: {
    name: '',
    nodes: [],
    id: '',
    updatedAt: '',
  },
  error: false,
  errorMsg: '',
  success: false,
  successMsg: '',
};

const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_FACTORIES:
      return Object.assign({}, state, { factories: action.data });
    case actionTypes.SELECT_FACTORY:
      return Object.assign({}, state, { selectedFactory: action.data });
    case actionTypes.SET_FACTORY_NAME:
      return Object.assign({}, state, {
        selectedFactory: {
          ...state.selectedFactory,
          name: action.data,
        },
      });
    case actionTypes.SET_ERROR:
      return Object.assign({}, state, {
        error: true,
        errorMsg: action.message,
        success: false,
      });
    case actionTypes.SET_SUCCESS:
      return Object.assign({}, state, {
        success: true,
        successMsg: action.message,
        error: false,
      });
    case actionTypes.CLEAR_NOTIFICATION:
      return Object.assign({}, state, {
        error: false,
        errorMsg: '',
        success: false,
        successMsg: '',
      });
    default:
      return state;
  }
};

export default mainReducer;
