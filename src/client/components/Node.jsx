import React from 'react';
import PropTypes from 'prop-types';

const Node = props => (
  <li>{props.value}</li>
);

Node.propTypes = {
  value: PropTypes.number.isRequired,
};

export default Node;
