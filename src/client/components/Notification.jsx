import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Notification extends Component {
  constructor() {
    super();

    this.timeout = null;
    this.handleClose = this.handleClose.bind(this);
  }

  componentDidMount() {
    this.timeout = setTimeout(() => { this.handleClose(); }, 3500);
  }

  componentWillUnmount() {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
  }

  handleClose() {
    this.props.close();
  }

  render() {
    return (
      <div onClick={this.props.close} className={`notification ${this.props.error ? 'error' : 'success'}`}>
        {this.props.message}
      </div>
    );
  }
}

Notification.propTypes = {
  close: PropTypes.func.isRequired,
  error: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
};

export default Notification;
