import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen } from '@fortawesome/free-solid-svg-icons';
import { setFactoryName, setError } from '../stores/actions';
import { validateEditFactory } from '../utils/validator';

class FactoryUpdater extends Component {
  constructor() {
    super();

    this.handleInputTyping = this.handleInputTyping.bind(this);
    this.handleEditClick = this.handleEditClick.bind(this);
    this.handleLeaveInput = this.handleLeaveInput.bind(this);
    this.handleCheckboxInput = this.handleCheckboxInput.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);

    this.state = {
      name: '',
      editName: false,
      removed: [],
      nodeCount: 0,
      low: 0,
      high: 1000,
    };
  }

  handleInputTyping(e) {
    const { value, name } = e.target;
    this.setState({ [name]: value });
  }

  handleEditClick() {
    this.setState({
      name: this.props.selectedFactory.name,
      editName: true,
    });
  }

  handleLeaveInput() {
    this.setState({
      editName: false,
    });
    this.props.setFactoryName(this.state.name);
  }

  handleCheckboxInput(e, node) {
    if (e.target.checked) {
      this.setState(prevState => ({
        removed: [...prevState.removed, node],
      }));
    } else {
      const oldNodes = this.state.removed;
      const index = oldNodes.indexOf(node);
      const newNodes = oldNodes.splice(index, 1);
      this.setState({
        removed: newNodes,
      });
    }
  }

  handleUpdate() {
    const {
      removed, nodeCount, low, high,
    } = this.state;

    const editedFactory = {
      name: this.props.selectedFactory.name,
      removed,
      nodeCount,
      low,
      high,
    };

    const errors = validateEditFactory(editedFactory, this.props.selectedFactory.nodes);

    if (errors.length) {
      this.props.setError(errors.join(', '));
    } else {
      this.props.update(editedFactory);
      this.setState({
        name: '',
        editName: false,
        removed: [],
        nodeCount: 0,
        low: 0,
        high: 1000,
      });
    }

  }

  render() {
    const { name, nodes, updatedAt } = this.props.selectedFactory;
    return (
      <form>
        {!this.state.editName
          ? (
            <h3>
              {name} <FontAwesomeIcon size="xs" onClick={this.handleEditClick} icon={faPen} />
            </h3>
          )
          : (
            <input
              className="typing-input"
              name="name"
              value={this.state.name}
              onChange={this.handleInputTyping}
              onClick={this.handleInputClick}
              onBlur={this.handleLeaveInput}
            />
          )
        }
        <span className="timestamp">last updated: {updatedAt}</span>
        <div className="form-group">
          <label>Select Nodes To Delete</label>
          {nodes.map(node => (
            <div className="node-checkbox" key={node.id}>
              <input
                onChange={e => this.handleCheckboxInput(e, node.value)}
                type="checkbox"
                value={node.value}
              />
              <label className="node-label" >{node.value}</label>
            </div>
          ))}
        </div>
        <div className="form-group">
          <label>
            Add Nodes
            <input
              className="typing-input"
              type="number"
              name="nodeCount"
              max={15 - nodes.length + this.state.removed.length}
              defaultValue={this.state.nodeCount}
              onChange={this.handleInputTyping}
            />
          </label>
        </div>
        <div className="form-group">
          <label>Number Range</label>
          <input
            className="typing-input"
            onChange={this.handleInputTyping}
            name="low"
            defaultValue={this.state.low}
            placeholder="Low"
          />
          <input
            className="typing-input"
            onChange={this.handleInputTyping}
            name="high"
            defaultValue={this.state.high}
            placeholder="High"
          />
        </div>
        <div className="form-group">
          <button onClick={this.handleUpdate} type="button">submit</button>
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  selectedFactory: state.selectedFactory,
});

const mapDispatchToProps = dispatch => ({
  setFactoryName: bindActionCreators(setFactoryName, dispatch),
  setError: bindActionCreators(setError, dispatch),
});

FactoryUpdater.propTypes = {
  update: PropTypes.func.isRequired,
  setFactoryName: PropTypes.func.isRequired,
  setError: PropTypes.func.isRequired,
  selectedFactory: PropTypes.shape({
    name: PropTypes.string.isRequired,
    nodes: PropTypes.arrayOf(PropTypes.object).isRequired,
    id: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
  }).isRequired,
};

export default connect(mapStateToProps, mapDispatchToProps)(FactoryUpdater);
