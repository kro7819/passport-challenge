import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { validateCreateFactory } from '../utils/validator';

class FactoryCreator extends Component {
  constructor() {
    super();

    this.handleInputChange = this.handleInputChange.bind(this);
    this.createNewFactory = this.createNewFactory.bind(this);
    this.handleKeyUp = this.createNewFactory.bind(this);

    this.state = {
      name: '',
      nodeCount: 1,
      low: 0,
      high: 1000,
    };
  }

  handleInputChange(e) {
    const { value, name } = e.target;
    this.setState({ [name]: value });
  }

  handleKeyUp(e) {
    if (e.key === 'Enter') {
      this.createNewFactory(e);
    }
  }

  createNewFactory(e) {
    e.preventDefault();
    const errors = validateCreateFactory(this.state);
    if (errors.length) {
      this.props.setError(errors.join(', '));
    } else {
      const {
        name, nodeCount, low, high,
      } = this.state;
      const body = {
        name,
        nodeCount: parseInt(nodeCount, 10),
        low: parseInt(low, 10),
        high: parseInt(high, 10),
      };
      this.props.create(body);
    }
  }

  render() {
    return (
      <form>
        <div>
          <label>Name</label>
          <input
            onChange={this.handleInputChange}
            name="name"
            placeholder="Name"
          />
        </div>
        <div>
          <label>Node Count</label>
          <input
            onChange={this.handleInputChange}
            name="nodeCount"
            defaultValue={this.state.nodeCount}
            placeholder="Node Count"
          />
        </div>
        <div>
          <label>Number Range</label>
          <input
            onChange={this.handleInputChange}
            name="low"
            defaultValue={this.state.low}
            placeholder="Low"
          />
          <input
            onChange={this.handleInputChange}
            name="high"
            defaultValue={this.state.high}
            placeholder="High"
          />
        </div>
        <div>
          <button onKeyDown={this.handleKeyUp} type="button" onClick={this.createNewFactory}>submit</button>

        </div>
      </form>
    );
  }
}

FactoryCreator.propTypes = {
  create: PropTypes.func.isRequired,
  setError: PropTypes.func.isRequired,
};

export default FactoryCreator;
