import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPen } from '@fortawesome/free-solid-svg-icons';
import Node from './Node';

const Factory = (props) => {
  const { name, nodes } = props.factory;
  return (
    <div className="factory">
      <div>
        <FontAwesomeIcon onClick={e => props.handleClick(e, props.factory)} size="sm" icon={faPen} />
      </div>
      <li>
        <h2 className="factory-name">{name}</h2>
        <ul className="factory-nodes">
          {nodes.map(node => (
            <Node key={node.id} value={node.value} />
          ))}
        </ul>
      </li>
    </div>
  );
};

Factory.propTypes = {
  handleClick: PropTypes.func.isRequired,
  factory: PropTypes.shape({
    name: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
    nodes: PropTypes.arrayOf(PropTypes.object).isRequired,
  }).isRequired,
};

export default Factory;
