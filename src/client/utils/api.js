class Api {
  constructor() {
    this.baseUrl = process.env.NODE_ENV === 'production' ? '/api' : 'http://localhost:4000/api';
    this.methods = {
      get: 'GET',
      post: 'POST',
      put: 'PUT',
      delete: 'DELETE',
    };
  }

  newRequest(method, route, body) {
    const url = `${this.baseUrl}${route}`;
    const data = {
      method,
      body: JSON.stringify(body) || null,
      headers: {
        'Content-Type': 'application/json',
      },
    };

    return fetch(url, data)
      .then((res) => {
        if (!res.ok) throw res;
        return res.json();
      });
  }

  getFactories() {
    return this.newRequest(this.methods.get, '/factories');
  }

  deleteFactory(id) {
    return this.newRequest(this.methods.delete, `/factories/${id}`)
      .catch(() => {
        throw new Error('Something went wrong when trying to delete factory');
      });
  }

  createFactory(body) {
    return this.newRequest(this.methods.post, '/factories', body)
      .catch((err) => {
        if (err.status === 409) {
          throw new Error('There is a factory with this name already!');
        }

        throw new Error('Something went wrong when trying to create factory');
      });
  }

  updateFactory(id, body) {
    return this.newRequest(this.methods.put, `/factories/${id}`, body)
      .catch((err) => {
        if (err.status === 409) {
          throw new Error('There is a factory with this name already!');
        }

        throw new Error('Something went wrong when trying to update factory');
      });
  }
}

module.exports = new Api();
