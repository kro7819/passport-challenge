// element validators
const validateName = (name) => {
  if (!name || name.length > 50) {
    return 'Name is required and should be under 50 characters';
  }

  return '';
};

const validateNodeCount = (nodeCount) => {
  if (nodeCount === '' || !parseInt(nodeCount, 10) || nodeCount > 15 || nodeCount < 1) {
    return 'Please enter a node count between 1 and 15 inclusive';
  }

  return '';
};

const validateAddNodes = (nodeCount, removed, nodes) => {
  if (nodeCount === '' || parseInt(nodeCount, 10) === 'NaN' || nodeCount > 15 || nodeCount < 0) {
    return 'Please enter a valid node count';
  }

  const totalNodes = +nodeCount - removed.length + nodes.length;

  if (totalNodes > 15) {
    return `Please add less nodes or remove more nodes: total is ${totalNodes - 15} over`;
  }

  return '';
};

const validateRange = (low, high) => {
  if (
    low >= high
    || low < 0
    || high > 1000000000
    || parseInt(low, 10) === 'NaN'
    || parseInt(high, 10) === 'NaN'
    || low === ''
    || high === ''
  ) return 'Please enter a valid number range';

  return '';
};

// form validators
export const validateCreateFactory = (form) => {
  const {
    name, nodeCount, low, high,
  } = form;

  const errors = [
    validateName(name),
    validateNodeCount(nodeCount),
    validateRange(low, high),
  ];

  return errors.filter(err => err !== '');
};

export const validateEditFactory = (form, nodes) => {
  const {
    name, removed, nodeCount, low, high,
  } = form;

  const errors = [
    validateName(name),
    validateAddNodes(nodeCount, removed, nodes),
    validateRange(low, high),
  ];

  return errors.filter(err => err !== '');
};
