import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import Factory from '../components/Factory';
import FactoryCreator from '../components/FactoryCreator';
import FactoryUpdater from '../components/FactoryUpdater';
import {
  deleteFactory, createFactory, updateFactory, selectFactory, setError,
} from '../stores/actions';

class Factories extends Component {
  constructor() {
    super();

    this.handleDeleteFactory = this.handleDeleteFactory.bind(this);
    this.handleCreateButton = this.handleCreateButton.bind(this);
    this.handleCancelButton = this.handleCancelButton.bind(this);
    this.handleCreateFactory = this.handleCreateFactory.bind(this);
    this.handleUpdateFactory = this.handleUpdateFactory.bind(this);
    this.handleFactoryClick = this.handleFactoryClick.bind(this);

    this.myRef = React.createRef();

    this.state = {
      createFactory: false,
      updateFactory: false,
    };
  }

  handleDeleteFactory(e) {
    e.preventDefault();
    this.props.deleteFactory(this.props.selectedFactory.id);
    this.setState({
      updateFactory: false,
    });
  }

  handleUpdateFactory(body) {
    const { id, nodes } = this.props.selectedFactory;
    if (nodes.length === body.removed.length) {
      this.props.deleteFactory(id);
    } else {
      console.log(body);
      const totalNodes = nodes.length + body.removed.length + body.nodes;
      if (totalNodes > 15) {
        body.nodes -= totalNodes - 15;
      }
      this.props.updateFactory(id, body);
    }
    this.setState({
      updateFactory: false,
    });
  }

  handleCreateFactory(body) {
    this.props.createFactory(body);
    this.setState({ createFactory: false });
  }

  handleCreateButton() {
    this.setState({
      createFactory: true,
      updateFactory: false,
    });
  }

  handleCancelButton(e) {
    e.preventDefault();
    this.setState({
      createFactory: false,
      updateFactory: false,
    });
  }

  handleFactoryClick(e, factory) {
    const domNode = this.myRef.current;
    domNode.scrollTo(0, 0);
    window.scroll({
      top: 0,
      behavior: 'auto',
    });
    this.setState({
      createFactory: false,
      updateFactory: true,
    });
    this.props.selectFactory(factory);
  }

  render() {
    return (
      <div className="factories content" ref={this.myRef}>
        {this.props.factories && this.props.factories.length ? (
          <div className="factories-view">
            <h1>Root</h1>
            <ul className="factories-view__factories">
              {this.props.factories.map(factory => (
                <Factory
                  handleClick={this.handleFactoryClick}
                  key={factory.id}
                  factory={factory}
                />))
              }
            </ul>
          </div>
        ) : (
          <div>
            <h2>There are currently no factories, please create one!</h2>
          </div>
        )
      }
        {!this.state.updateFactory && (
          <div className="factories-create">
            <div>
              <button onClick={this.handleCreateButton} type="button">create new factory</button>
            </div>
            {this.state.createFactory && (
              <div>
                <FactoryCreator create={this.handleCreateFactory} setError={this.props.setError} />
                <button type="button" onClick={this.handleCancelButton}>cancel</button>
              </div>
            )}
          </div>
        )}
        {!this.state.createFactory && this.state.updateFactory && (
          <div className="factories-update">
            <h2>Update Factory:</h2>
            <FactoryUpdater
              update={this.handleUpdateFactory}
            />
            <button type="button" onClick={this.handleDeleteFactory}>delete factory</button>
            <button type="button" onClick={this.handleCancelButton}>cancel</button>
          </div>
        )}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  deleteFactory: bindActionCreators(deleteFactory, dispatch),
  createFactory: bindActionCreators(createFactory, dispatch),
  updateFactory: bindActionCreators(updateFactory, dispatch),
  selectFactory: bindActionCreators(selectFactory, dispatch),
  setError: bindActionCreators(setError, dispatch),
});

const mapStateToProps = state => ({
  factories: state.factories,
  selectedFactory: state.selectedFactory,
});

Factories.propTypes = {
  deleteFactory: PropTypes.func.isRequired,
  createFactory: PropTypes.func.isRequired,
  updateFactory: PropTypes.func.isRequired,
  selectFactory: PropTypes.func.isRequired,
  setError: PropTypes.func.isRequired,
  factories: PropTypes.arrayOf(PropTypes.object),
  selectedFactory: PropTypes.shape({
    name: PropTypes.string.isRequired,
    nodes: PropTypes.arrayOf(PropTypes.object).isRequired,
    id: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
  }).isRequired,
};

Factories.defaultProps = {
  factories: [],
};


export default connect(mapStateToProps, mapDispatchToProps)(Factories);
