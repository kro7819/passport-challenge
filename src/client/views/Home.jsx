import React from 'react';

const Home = () => (
  <div className="home content">
    <h1>Welcome!</h1>
    <h2>Click on the dashboard link in the nav to interact</h2>
    <div>
      <h3>Functionality for factories includes:</h3>
      <ul>
        <li>Creating</li>
        <li>Editing</li>
        <li>Deleting</li>
      </ul>
    </div>
    <div>
      <h3>When creating factories:</h3>
      <ul>
        <li>Add up to 15 nodes</li>
        <li>node numbers can be between 0 and 1000000000</li>
      </ul>
    </div>
    <div>
      <h3>When editing factories:</h3>
      <ul>
        <li>Factory name can be edited</li>
        <li>Individual nodes can be removed and more nodes can be added to existing factories</li>
      </ul>
    </div>
  </div>
);

export default Home;
