const db = require('./');

module.exports = {
  factory: {
    getAll: () => {
      const query = `SELECT factories.id, name, nodes.node, nodes.id as node_id, factories.updated_at FROM factories
      INNER join nodes ON nodes.factory_id = factories.id
      ORDER BY name`;

      return db.query(query);
    },
    create: (name, nodes) => new Promise((resolve, reject) => {
      let connection;
      let id;
      db.getConnection()
        .then((conn) => {
          connection = conn;
          return connection.beginTransaction();
        })
        .then(() => (
          connection.query('INSERT INTO factories (name) VALUES (?)', name)
            .catch((err) => {
              connection.rollback()
                .then(() => {
                  connection.release();
                  return reject(err);
                });
            })
        ))
        .then(([results]) => {
          id = results.insertId;
          const rows = nodes.map(node => [id, node]);
          return connection.query('INSERT INTO nodes (factory_id, node) VALUES ?', [rows])
            .catch((err) => {
              connection.rollback()
                .then(() => {
                  connection.release();
                  return reject(err);
                });
            });
        })
        .then(() => (
          connection.commit()
            .catch((err) => {
              connection.rollback()
                .then(() => {
                  connection.release();
                  return reject(err);
                });
            })
        ))
        .then(() => {
          connection.release();
          resolve(id);
        });
    }),
    getById: (id) => {
      const query = `SELECT factories.id, name, nodes.node, nodes.id as node_id, factories.updated_at FROM factories
      INNER join nodes ON nodes.factory_id = factories.id
      WHERE factories.id = ?`;

      return db.query(query, id);
    },
    updateById: (id, body, added) => new Promise((resolve, reject) => {
      let connection;
      let { added } = body;
      const { removed, name } = body;

      return db.getConnection()
        .then((conn) => {
          connection = conn;
          return connection.beginTransaction();
        })
        .then(() => (
          connection.query('UPDATE factories SET name = ? WHERE id = ?', [name, id])
            .catch((err) => {
              connection.rollback()
                .then(() => {
                  connection.release();
                  return reject(err);
                });
            })
        ))
        .then(() => {
          if (!(removed && removed.length > 0)) return null;
          return connection.query('DELETE FROM nodes WHERE nodes.factory_id = ? AND nodes.node in (?)', [id, removed])
            .catch((err) => {
              connection.rollback()
                .then(() => {
                  connection.release();
                  return reject(err);
                });
            });
        })
        .then(() => {
          if (!(added && added.length > 0)) return null;

          added = added.map(add => [id, add]);
          console.log(added);
          return connection.query('INSERT INTO nodes (factory_id, node) VALUES ?', [added])
            .catch((err) => {
              connection.rollback()
                .then(() => {
                  connection.release();
                  return reject(err);
                });
            });
        })
        .then(() => (
          connection.commit()
            .catch((err) => {
              connection.rollback()
                .then(() => {
                  connection.release();
                  return reject(err);
                });
            })
        ))
        .then(() => {
          connection.release();
          resolve({});
        });
    }),
    deleteById: id => new Promise((resolve, reject) => {
      let connection;

      return db.getConnection()
        .then((conn) => {
          connection = conn;
          return connection.beginTransaction();
        })
        .then(() => (
          connection.query('DELETE FROM factories WHERE id = ?', id)
            .catch((err) => {
              connection.rollback()
                .then(() => {
                  connection.release();
                  return reject(err);
                });
            })
        ))
        .then(() => (
          connection.query('DELETE FROM nodes WHERE factory_id = ?', id)
            .catch((err) => {
              connection.rollback()
                .then(() => {
                  connection.release();
                  return reject(err);
                });
            })
        ))
        .then(() => (
          connection.commit()
            .catch((err) => {
              connection.rollback()
                .then(() => {
                  connection.release();
                  return reject(err);
                });
            })
        ))
        .then(() => {
          connection.release();
          return resolve();
        });
    }),
  },
};
